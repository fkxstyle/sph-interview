<?php

use App\PollutantStandardIndex;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class PollutantStandardIndexTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $todayDate = Carbon::now();
        PollutantStandardIndex::getFromDataGovSgAndStore($todayDate->format('Y-m-d'));
    }
}
