<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePollutantStandardIndexesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pollutant_standard_indexes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('date');
            $table->string('region_id');
            $table->double('pm10_twenty_four_hourly')->default(0);
            $table->double('pm25_twenty_four_hourly')->default(0);
            $table->double('co_sub_index')->default(0);
            $table->double('o3_sub_index')->default(0);
            $table->double('so2_sub_index')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pollutant_standard_indexes');
    }
}
