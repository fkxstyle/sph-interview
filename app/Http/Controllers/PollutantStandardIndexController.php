<?php

namespace App\Http\Controllers;

use App\AirTemperature;
use App\PollutantStandardIndex;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class PollutantStandardIndexController extends BaseController
{
    use ValidatesRequests;

    public function index(Request $request)
    {
        $request->validate([
            'date' => 'required|date_format:Y-m-d',
        ]);

        $date = $request->date;
        
        // if not found in db, get from data.gov.sg
        if (
            !PollutantStandardIndex::where('date', $date)->exists()
        ) {
            PollutantStandardIndex::getFromDataGovSgAndStore($date);
        }

        // if not found in db, get from data.gov.sg
        if (
            !AirTemperature::where('date_time', 'like', $date . '%')->exists()
        ) {
            AirTemperature::getFromDataGovSgAndStore($date);
        }

        return [
            'psi' => PollutantStandardIndex::where('date', $date)->get(),
            'air_temperature' => AirTemperature::where('date_time', 'like', $date . '%')->get(),
        ];
    }
}
