<?php

namespace App\Http\Controllers;

use App\AirTemperature;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class AirTemperatureController extends BaseController
{
    use ValidatesRequests;

    public function index(Request $request)
    {
        $request->validate([
            'date' => 'required|date_format:Y-m-d',
        ]);

        $date = $request->date;
        
        // if not found in db, get from data.gov.sg
        if (
            !AirTemperature::where('date_time', 'like', $date . '%')->exists()
        ) {
            AirTemperature::getFromDataGovSgAndStore($date);
        }

        return AirTemperature::where('date_time', 'like', $date . '%')->get();
    }
}
