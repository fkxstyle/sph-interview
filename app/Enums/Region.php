<?php

namespace App\Enums;

class Region
{
    const ID_NATIONAL = 1;
    const ID_NORTH = 2;
    const ID_SOUTH = 3;
    const ID_EAST = 4;
    const ID_WEST = 5;

    const NAME_NATIONAL = 'national';
    const NAME_NORTH = 'north';
    const NAME_SOUTH = 'south';
    const NAME_EAST = 'east';
    const NAME_WEST = 'west';

    const REGION = [
        self::ID_NATIONAL => self::NAME_NATIONAL,
        self::ID_NORTH => self::NAME_NORTH,
        self::ID_SOUTH => self::NAME_SOUTH,
        self::ID_EAST => self::NAME_EAST,
        self::ID_WEST => self::NAME_WEST,
    ];
}