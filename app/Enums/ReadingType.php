<?php

namespace App\Enums;

class ReadingType
{
    const ID_PM10_TWENTY_FOUR_HOURLY = 1;
    const ID_PM25_TWENTY_FOUR_HOURLY = 2;
    const ID_CO_SUB_INDEX = 3;
    const ID_O3_SUB_INDEX = 4;
    const ID_SO2_SUB_INDEX = 5;

    const NAME_PM10_TWENTY_FOUR_HOURLY = 'pm10_twenty_four_hourly';
    const NAME_PM25_TWENTY_FOUR_HOURLY = 'pm25_twenty_four_hourly';
    const NAME_CO_SUB_INDEX = 'co_sub_index';
    const NAME_O3_SUB_INDEX = 'o3_sub_index';
    const NAME_SO2_SUB_INDEX = 'so2_sub_index';

    const READING_TYPE = [
        self::ID_PM10_TWENTY_FOUR_HOURLY => self::NAME_PM10_TWENTY_FOUR_HOURLY,
        self::ID_PM25_TWENTY_FOUR_HOURLY => self::NAME_PM25_TWENTY_FOUR_HOURLY,
        self::ID_CO_SUB_INDEX => self::NAME_CO_SUB_INDEX,
        self::ID_O3_SUB_INDEX => self::NAME_O3_SUB_INDEX,
        self::ID_SO2_SUB_INDEX => self::NAME_SO2_SUB_INDEX,
    ];
}