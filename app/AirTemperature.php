<?php

namespace App;

use App\Enums\ReadingType;
use App\Enums\Region;
use App\Libraries\DataGovSgService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AirTemperature extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'air_temperatures';

    protected $fillable = [
        'date_time',
        'station_id',
        'name',
        'value',
    ];

    /**
     * @date in YYYY-MM-DD format
     */
    public static function getFromDataGovSgAndStore(string $date)
    {
        $airTemperatures = (new DataGovSgService())->getAirTemperatures($date);

        $dateTime = Carbon::parse($airTemperatures['timestamp']);
        $dateTime = $dateTime->format('Y-m-d H:i:s');

        // Assuming the first reading is always S109
        $reading = $airTemperatures['readings'][0];
        
        $airTemperature = AirTemperature::updateOrCreate(
            [
                'date_time' => $dateTime,
                'station_id' => $reading['station_id'],
            ],
            [
                'date_time' => $dateTime,
                'station_id' => $reading['station_id'],
                'name' => 'Ang Mo Kio Avenue 5',
                'value' => $reading['value'],
            ]
        );

        return $airTemperature;
    }
}
