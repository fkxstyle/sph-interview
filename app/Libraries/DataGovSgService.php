<?php

namespace App\Libraries;

use GuzzleHttp\Client;

class DataGovSgService
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.data.gov.sg/v1/',
        ]);
    }

    public function getPSI(string $date)
    {
        $response = $this->client->get(
            'environment/psi?date=' . $date
        );
        $body = $response->getBody();
        return json_decode($body->getContents(), true)['items'][0]['readings'];
    }

    public function getAirTemperatures(string $date)
    {
        $response = $this->client->get(
            'environment/air-temperature?date=' . $date
        );
        $body = $response->getBody();
        // Only take the first result
        return json_decode($body->getContents(), true)['items'][0];
    }
}