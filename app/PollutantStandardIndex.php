<?php

namespace App;

use App\Enums\ReadingType;
use App\Enums\Region;
use App\Libraries\DataGovSgService;
use Illuminate\Database\Eloquent\Model;

class PollutantStandardIndex extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pollutant_standard_indexes';

    protected $fillable = [
        'date',
        'region_id',
        'pm10_twenty_four_hourly',
        'pm25_twenty_four_hourly',
        'co_sub_index',
        'o3_sub_index',
        'so2_sub_index',
    ];

    /**
     * @date in YYYY-MM-DD format
     */
    public static function getFromDataGovSgAndStore(string $date)
    {
        $psi = (new DataGovSgService())->getPSI($date);

        // Only store based on relevant reading type and region
        foreach (Region::REGION as $regionId => $regionName) {

            // Store all psi accordingly
            PollutantStandardIndex::updateOrCreate(
                [
                    'date' => $date,
                    'region_id' => $regionId,
                ],
                [
                    'date' => $date,
                    'region_id' => $regionId,
                    'pm10_twenty_four_hourly' => $psi['pm10_twenty_four_hourly'][$regionName],
                    'pm25_twenty_four_hourly' => $psi['pm25_twenty_four_hourly'][$regionName],
                    'co_sub_index' => $psi['co_sub_index'][$regionName],
                    'o3_sub_index' => $psi['o3_sub_index'][$regionName],
                    'so2_sub_index' => $psi['so2_sub_index'][$regionName],
                ]
            );
        }
        
        return $psi;
    }
}
