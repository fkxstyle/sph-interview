# SPH Assignment by Foo Kian Xiong #

This README would normally document whatever steps are necessary to get your application up and running.

### Sample Demo URL ###
* https://lit-reef-68308.herokuapp.com/

### How do I get set up? ###

* copy .env.example and rename it to .env
* change APP_URL to http://sph.interview.com
* add http://sph.interview.com to your host file and map to localhost IP
* Run following comment
* - composer install
* - php artisan migrate
