<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PollutantStandardIndexControllerTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexPsi()
    {
        $response = $this->get('/api/psi?date=2020-06-08');
        $this->assertNotEmpty($response->json());
        $response->assertStatus(200);
    }
}
