<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AirTemperatureControllerTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndexAirTemperature()
    {
        $response = $this->get('/api/air-temperature?date=2020-06-08');
        $this->assertNotEmpty($response->json());
        $response->assertStatus(200);
    }
}
