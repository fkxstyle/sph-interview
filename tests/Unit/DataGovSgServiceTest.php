<?php

namespace Tests\Unit;

use App\Libraries\DataGovSgService;
use PHPUnit\Framework\TestCase;

class DataGovSgServiceTest extends TestCase
{

    public function testGetPsi()
    {
        $psi = (new DataGovSgService())->getPSI('2020-01-01');
        
        $this->assertArrayHasKey('o3_sub_index', $psi);
        $this->assertArrayHasKey('pm10_twenty_four_hourly', $psi);
        $this->assertArrayHasKey('pm10_sub_index', $psi);
    }

    public function testGetAirTemperature()
    {
        $airTemperatures = (new DataGovSgService())->getAirTemperatures('2020-06-08');
        $this->assertArrayHasKey('timestamp', $airTemperatures);
        $this->assertArrayHasKey('readings', $airTemperatures);

        $airTemperature = $airTemperatures['readings'][0];
        $this->assertArrayHasKey('station_id', $airTemperature);
        $this->assertArrayHasKey('value', $airTemperature);
    }
}
