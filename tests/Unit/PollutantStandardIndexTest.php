<?php

namespace Tests\Feature;

use App\PollutantStandardIndex;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PollutantStandardIndexTest extends TestCase
{
    // use DatabaseTransactions;
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetFromDataGovSg()
    {
        $psi = PollutantStandardIndex::getFromDataGovSgAndStore('2020-01-01');

        $this->assertTrue(
            PollutantStandardIndex::where('date', '2020-01-01')->exists()
        );
    }
}
