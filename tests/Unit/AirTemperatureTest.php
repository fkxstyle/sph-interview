<?php

namespace Tests\Feature;

use App\AirTemperature;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AirTemperatureTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetFromDataGovSg()
    {
        AirTemperature::getFromDataGovSgAndStore('2020-06-08');

        $this->assertTrue(
            AirTemperature::where('station_id', 'S109')->exists()
        );
    }
}
